import java.util.Arrays;

public class Main {
    enum Ordering {
        Less,
        Equal,
        Greater,
    }

    public static Ordering compare(String a, String b) {
        for (int i = 0; i < Integer.min(a.length(), b.length()); i++) {
            if (a.charAt(i) > b.charAt(i)) {
                return Ordering.Greater;
            } else if (a.charAt(i) < b.charAt(i)) {
                return Ordering.Less;
            }
        }

        var deltaLenght = a.length() - b.length();

        if (deltaLenght == 0) {
            return Ordering.Equal;
        } else if (deltaLenght > 0) {
            return Ordering.Greater;
        } else {
            return Ordering.Less;
        }
    }

    public static void directSelectionSort(String[] array) {
        for (int i = 0; i < array.length; i++) {
            String min = null;
            int index = i;

            for (int j = i + 1; j < array.length; j++) {
                if (compare(array[j], array[i]) == Ordering.Less) {
                    if (min != null) {
                        if (compare(array[j], min) == Ordering.Greater) {
                            continue;
                        }
                    }

                    min = array[j];
                    index = j;
                }
            }

            var temp = array[i];
            array[i] = array[index];
            array[index] = temp;
        }
    }

    public static void bubbleSort(String[] array) {
        boolean wasSwapped;

        do {
            wasSwapped = false;

            for (int i = 0; i < array.length - 1; i++) {
                if (compare(array[i], array[i + 1]) == Ordering.Greater) {
                    var temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;

                    wasSwapped = true;
                }
            }
        } while (wasSwapped);
    }

    public static void insertionSort(String[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i; j >= 0; j--) {
                if (compare(array[j + 1], array[j]) == Ordering.Greater) {
                    break;
                }

                var temp = array[j + 1];
                array[j + 1] = array[j];
                array[j] = temp;
            }
        }
    }

    public static void main(String[] args) {
        System.out.println(compare("Aboba", "Abobav2") == Ordering.Less);
        System.out.println(compare("AmongSus", "AmongSus") == Ordering.Equal);
        System.out.println(compare("Acbd", "Abcd") == Ordering.Greater);

        final String[] originalArray = {
                "Ivanov",
                "Petrov",
                "Albert",
                "Petrov2",
                "Ivanov",
                "Petya",
                "Oleg",
        };

        System.out.println("Original:\t\t" + Arrays.toString(originalArray));

        final String[] rightAnswer = {
                "Albert",
                "Ivanov",
                "Ivanov",
                "Oleg",
                "Petrov",
                "Petrov2",
                "Petya",
        };

        System.out.println("Right answer:\t" + Arrays.toString(rightAnswer));

        var array = originalArray.clone();
        directSelectionSort(array);
        System.out.println(Arrays.toString(array));
        System.out.println(Arrays.equals(array, rightAnswer));

        array = originalArray.clone();
        bubbleSort(array);
        System.out.println(Arrays.toString(array));
        System.out.println(Arrays.equals(array, rightAnswer));

        array = originalArray.clone();
        insertionSort(array);
        System.out.println(Arrays.toString(array));
        System.out.println(Arrays.equals(array, rightAnswer));
    }
}
